# QWIC Coding Assessment

This repository has both the frontend and the backend part of the coding assessment together.

## Prerequisites
* Yarn (> V1.0.0)
* Node (> V10.0.0)

## Commands

`yarn install` Installs the dependencies of both frontend and backend

`yarn client` Runs the client application alone

`yarn server` Runs the server application alone

`yarn start` Runs both the server and the client together

## Installation & Running

* When you are in the root directory of the project, run `yarn install` first to get all the required dependencies
* Run `yarn start` to run both server and client together.
* Open your favorite browser (not IE, please) and open `http://localhost:3000` to see the frontend
* Open another tab and type `http://localhost:3001/explorer/` to see/try the available api endpoints and documentation 


* *(Optional)*  Run `yarn client:build` to build frontend project
* *(Optional)*  Run `yarn client:test` to run snapshot tests of the frontend
* *(Optional)*  Run `yarn client:cypress` to run cypress E2E tests of the frontend


* *(Optional)*  Run `yarn server:build` to build backend project
* *(Optional)*  Run `yarn server:test` to run backend tests

## Possible improvements

* We can add more and more tests
* We can add some unhappy flow tests into cypress
* Backend almost has no tests since it is so simple and using an external library, but we can add more tests into backend for sure

