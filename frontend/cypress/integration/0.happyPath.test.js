describe("Expected happy flow", () => {
  it("Visit the form", () => {
    cy.visit("/", { timeout: 10000 });
  });

  it("Fill values into the form", () => {
    // Create an interceptor for actual service call
    cy.intercept({
      method: "POST",
      url: "http://localhost:3001/return-requests"
    }).as("returnRequests");

    cy.fixture("return-request.json").then((returnRequest) => {
      const { serialNumber, reason, reasonDescription, invoiceNumber, comments } = returnRequest;

      // Fill form fields
      cy.get('input[data-test-id="serial-number-input"]').type(serialNumber);

      // Select "other" option from "reason" select
      cy.get('div[data-test-id="reason-input"]').click();
      cy.get('div[id="menu-reason"] ul li:nth(2)').click();

      // Fill form fields
      cy.get('input[data-test-id="reason-description-input"]').type(reasonDescription);
      cy.get('input[data-test-id="invoice-number-input"]').type(invoiceNumber);
      cy.get('textarea[data-test-id="comments-input"]').type(comments);

      // Click the submit button
      cy.get('button[data-test-id="submit-button"]').click();

      // Checking the result coming from the server
      cy.wait("@returnRequests")
        .its("response.statusCode")
        .should("equal", 200);

      cy.get("@returnRequests")
        .its("request.body")
        .should("deep.equal", returnRequest);

      cy.get("@returnRequests")
        .should(({ response }) => {
          expect(response.body.serialNumber).eq(serialNumber);
          expect(response.body.reason).eq(reason);
          expect(response.body.reasonDescription).eq(reasonDescription);
          expect(response.body.invoiceNumber).eq(invoiceNumber);
          expect(response.body.comments).eq(comments);
        });

      // Check if success modal is visible
      cy.get('div[data-test-id="alert-dialog-view"]').should("exist");
    });
  });
})
