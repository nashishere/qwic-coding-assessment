import React, {useCallback, useRef, useState} from "react";
import {Container, Divider, Typography} from "@material-ui/core";
import CssBaseline from "@material-ui/core/CssBaseline";

import {MOCK_DATA} from "./utils/Constants";
import NetworkManager from "./utils/NetworkManager";
import PartDetailView from "./components/PartDetailView";
import ReturnFormView from "./components/ReturnFormView";
import AlertDialogView from "./components/AlertDialogView";

import "./Application.scss";

const Application = () => {
  const formRef = useRef();

  const [isLoading, setIsLoading] = useState(false);
  const [isAlertVisible, setIsAlertVisible] = useState(false);
  const [alertTitle, setAlertTitle] = useState("");
  const [alertDescription, setAlertDescription] = useState("");

  const handleOnSubmit = useCallback((data) => {
    // Disabling form buttons for eliminating accidental clicks
    setIsLoading(true);

    // Sending data to the server
    NetworkManager.postReturnRequest(data)
      .then(() => {
        // Resetting form values to default
        formRef.current?.reset();

        // Setting alert title and description (on success)
        setAlertTitle("Success");
        setAlertDescription("Your return request has successfully saved. We will reach you about your request within 3 working days.");
        setIsAlertVisible(true);
      })
      .catch(() => {
        // Setting alert title and description (on error)
        setAlertTitle("Error");
        setAlertDescription("Operation failed! Please try again later!");
        setIsAlertVisible(true);
      })
      .finally(() => {
        // Re-enabling form buttons
        setIsLoading(false);
      });
  }, []);

  return (
    <>
      <CssBaseline />
      <Container maxWidth="md">
        <Typography variant="h5" component="h1">Return Request</Typography>
        <PartDetailView data={MOCK_DATA.FORM} />
        <Divider />
        <ReturnFormView ref={formRef} isLoading={isLoading} onSubmit={handleOnSubmit} />

        <AlertDialogView
          isVisible={isAlertVisible}
          title={alertTitle}
          description={alertDescription}
          closeButtonTitle="Close"
          closeButtonAction={() => setIsAlertVisible(false)}
        />
      </Container>
    </>
  );
};

export default Application;
