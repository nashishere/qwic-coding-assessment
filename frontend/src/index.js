import React from "react";
import ReactDOM from "react-dom";

import Application from "./Application";

import "./index.scss";

const component = <Application />;

const container = document.getElementById("root");

ReactDOM.render(component, container);
