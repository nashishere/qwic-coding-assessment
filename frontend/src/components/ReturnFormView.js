import React, {forwardRef, useEffect, useMemo} from "react";
import PropTypes from "prop-types";
import {Box, Button, Grid, MenuItem, TextField, Typography} from "@material-ui/core";
import {useForm, Controller} from "react-hook-form";
import InputMask from "react-input-mask";

import {REASON} from "../utils/Constants";
import {getValidationRules} from "../utils/Utils";

import "./ReturnFormView.scss";

const ReturnFormView = forwardRef(({ isLoading = false, onSubmit }, reference) => {
  const {handleSubmit, control, watch, errors, reset} = useForm({ mode: "onBlur" });

  const reason = watch('reason');
  const isReasonDetailRequired = reason === REASON.OTHER;

  // Memoizing validation rules to eliminate unnecessary render cycles
  const validationRules = useMemo(() => getValidationRules(isReasonDetailRequired), [isReasonDetailRequired]);

  useEffect(() => {
    // Setting the reference object on mount
    if (reference) {
      reference.current = Object.freeze({ reset });
    }

    // Removing the reference on unmount
    return () => {
      if (reference) {
        reference.current = undefined;
      }
    };
  }, [reference]);

  return (
    <Box className="return-form" data-test-id="return-form-view">
      <Box className="return-form__title">
        <Typography variant="body1">Please provide us some details</Typography>
      </Box>

      <form onSubmit={handleSubmit(onSubmit)}>
        <Grid container className="return-form__grid" spacing={4}>
          <Grid item xs={12}>
            <Controller
              name="serialNumber"
              control={control}
              rules={validationRules.serialNumber}
              defaultValue=""
              render={(props) => (
                <InputMask mask="**********" maskChar="" value={props.value} onChange={props.onChange}>
                  {(inputProps) => (
                    <TextField
                      {...inputProps}
                      inputProps={{
                        "data-test-id": "serial-number-input",
                      }}
                      error={!!(errors.serialNumber)}
                      helperText={errors.serialNumber?.message}
                      fullWidth
                      variant="outlined"
                      label="Serial Number *"
                      placeholder="Please provide us your part's serial number"
                    />
                  )}
                </InputMask>
              )}
            />
          </Grid>

          <Grid item xs={12}>
            <Controller
              name="reason"
              data-test-id="reason-input"
              as={TextField}
              control={control}
              rules={validationRules.reason}
              defaultValue=""
              error={!!(errors.reason)}
              helperText={errors.reason?.message}
              fullWidth
              select
              variant="outlined"
              label="Reason *"
            >
              <MenuItem value={REASON.BROKEN}>Broken part</MenuItem>
              <MenuItem value={REASON.UNUSED}>Unused part</MenuItem>
              <MenuItem value={REASON.OTHER}>Other</MenuItem>
            </Controller>
          </Grid>

          {isReasonDetailRequired && (
            <Grid item xs={12}>
              <Controller
                name="reasonDescription"
                inputProps={{
                  "data-test-id": "reason-description-input",
                }}
                as={TextField}
                control={control}
                rules={validationRules.reasonDescription}
                defaultValue=""
                error={!!(errors.reasonDescription)}
                helperText={errors.reasonDescription?.message}
                fullWidth
                variant="outlined"
                label="Reason Description *"
                placeholder="Please provide us your reason to return the item"
              />
            </Grid>
          )}

          <Grid item xs={12}>
            <Controller
              name="invoiceNumber"
              control={control}
              rules={validationRules.invoiceNumber}
              defaultValue=""
              render={(props) => (
                <InputMask mask="******" maskChar="" value={props.value} onChange={props.onChange}>
                  {(inputProps) => (
                    <TextField
                      {...inputProps}
                      inputProps={{
                        "data-test-id": "invoice-number-input",
                      }}
                      error={!!(errors.invoiceNumber)}
                      helperText={errors.invoiceNumber?.message}
                      fullWidth
                      variant="outlined"
                      label="Invoice Number *"
                      placeholder="Please provide us your part's invoice number"
                    />
                  )}
                </InputMask>
              )}
            />
          </Grid>

          <Grid item xs={12}>
            <Controller
              name="comments"
              inputProps={{
                "data-test-id": "comments-input",
                maxLength: 240,
              }}
              as={TextField}
              control={control}
              defaultValue=""
              fullWidth
              multiline
              rows={4}
              variant="outlined"
              label="Comments"
            />
          </Grid>

          <Grid item xs={6}>
            <Button fullWidth disabled={isLoading} size="large" variant="outlined" onClick={reset}>Reset</Button>
          </Grid>

          <Grid item xs={6}>
            <Button
              data-test-id="submit-button"
              fullWidth
              disabled={isLoading}
              type="submit"
              size="large"
              variant="outlined"
            >
              Submit
            </Button>
          </Grid>
        </Grid>
      </form>
    </Box>
  );
});

ReturnFormView.propTypes = {
  reference: PropTypes.object,
  isLoading: PropTypes.bool,
  onSubmit: PropTypes.func.isRequired,
};

ReturnFormView.displayName = "ReturnFormView";

export default ReturnFormView;
