import React from "react";
import {render, cleanup} from "@testing-library/react";

import {MOCK_DATA} from "../utils/Constants";
import PartDetailView from "./PartDetailView";

describe("Part Detail View", () => {
  afterEach(cleanup);

  test("should render correctly with given parameters", async () => {
    const { baseElement } = render(<PartDetailView data={MOCK_DATA.FORM} />);

    expect(baseElement.querySelector('div[data-test-id="part-detail-view"]')).toMatchSnapshot();
  });
});
