import React from "react";
import {render, cleanup} from "@testing-library/react";

import ReturnFormView from "./ReturnFormView";

describe("Return Form View", () => {
  afterEach(cleanup);

  test("should render correctly with given parameters", async () => {
    const { baseElement } = render(
      <ReturnFormView
        isLoading={false}
        reference={null}
        onSubmit={() => {}}
      />
    );

    expect(baseElement.querySelector('div[data-test-id="return-form-view"]')).toMatchSnapshot();
  });
});
