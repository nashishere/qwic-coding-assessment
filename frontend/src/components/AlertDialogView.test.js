import React from "react";
import {render, cleanup} from "@testing-library/react";

import AlertDialogView from "./AlertDialogView";

describe("Alert Dialog View", () => {
  afterEach(cleanup);

  test("should render correctly with given parameters", async () => {
    const { baseElement } = render(
      <AlertDialogView
        isVisible={true}
        title="Foo"
        description="Bar"
        closeButtonTitle="Close"
        closeButtonAction={() => {}}
      />
    );

    expect(baseElement.querySelector('div[data-test-id="alert-dialog-view"]')).toMatchSnapshot();
  });
});
