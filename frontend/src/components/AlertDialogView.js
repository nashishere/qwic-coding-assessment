import React from "react";
import PropTypes from "prop-types";
import {Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle} from "@material-ui/core";

import "./AlertDialogView.scss";

const AlertDialogView = ({
  isVisible = false,
  title,
  description,
  closeButtonTitle,
  closeButtonAction,
  approveButtonTitle,
  approveButtonAction,
}) => {
  return (
    <Dialog
      data-test-id="alert-dialog-view"
      open={isVisible}
      onClose={closeButtonAction}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
      {description && (
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {description}
          </DialogContentText>
        </DialogContent>
      )}
      <DialogActions>
        <Button onClick={closeButtonAction} color="primary">
          {closeButtonTitle}
        </Button>

        {approveButtonTitle && approveButtonAction && (
          <Button onClick={approveButtonAction} color="primary" autoFocus>
            {approveButtonTitle}
          </Button>
        )}
      </DialogActions>
    </Dialog>
  );
};

AlertDialogView.propTypes = {
  isVisible: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string,
  closeButtonTitle: PropTypes.string.isRequired,
  closeButtonAction: PropTypes.func.isRequired,
  approveButtonTitle: PropTypes.string,
  approveButtonAction: PropTypes.func,
};

export default AlertDialogView;
