import React from "react";
import PropTypes from "prop-types";
import {Box, Grid, Typography} from "@material-ui/core";

import "./PartDetailView.scss";

const PartDetailView = ({ data }) => {
  return (
    <Box className="part-detail" data-test-id="part-detail-view">
      <Typography variant="body1">{data.bikeModel}</Typography>
      <Typography variant="body2">{data.bikeSerialNumber}</Typography>

      <Grid container className="part-detail__grid" justify="space-between">
        <Grid item>
          <Typography variant="subtitle2" component="p">{`# ${data.partNumber}`}</Typography>
          <Typography variant="body1">{data.partName}</Typography>
        </Grid>
        <Grid item>
          <img src={data.partImageUrl} alt={data.partName} />
        </Grid>
      </Grid>
    </Box>
  );
};

PartDetailView.propTypes = {
  data: PropTypes.shape({
    bikeModel: PropTypes.string.isRequired,
    bikeSerialNumber: PropTypes.string.isRequired,
    partNumber: PropTypes.string.isRequired,
    partName: PropTypes.string.isRequired,
    partImageUrl: PropTypes.string.isRequired,
  }).isRequired,
};

export default PartDetailView;
