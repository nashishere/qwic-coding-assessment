export const getValidationRules = (isReasonDetailRequired) => ({
  serialNumber: {
    required: {
      value: true,
      message: "This field is required.",
    },
    minLength: {
      value: 6,
      message: "This field should be at least 6 characters."
    },
    maxLength: {
      value: 10,
      message: "This field cannot be more than 10 characters."
    },
  },
  reason: {
    required: {
      value: true,
      message: "This field is required.",
    },
  },
  reasonDescription: {
    required: {
      value: isReasonDetailRequired,
      message: "This field is required.",
    },
    maxLength: {
      value: isReasonDetailRequired ? 240 : Number.MAX_VALUE,
      message: "This field cannot be more than 240 characters."
    },
  },
  invoiceNumber: {
    required: {
      value: true,
      message: "This field is required.",
    },
    maxLength: {
      value: 6,
      message: "This field cannot be more than 6 characters."
    },
  },
  comments: {
    maxLength: {
      value: 240,
      message: "This field cannot be more than 240 characters."
    },
  },
});
