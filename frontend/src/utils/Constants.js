export const MOCK_DATA = {
  FORM: {
    bikeModel: "QWIC PERF RD10 DT DIAMOND DUTCH ORANGE",
    bikeSerialNumber: "HP00760",
    partNumber: "11304",
    partName: "Acculader 4A Downtube",
    partImageUrl: "https://via.placeholder.com/40?text=4A",
  }
};

export const REASON = {
  BROKEN: "broken",
  UNUSED: "unused",
  OTHER: "other",
};
