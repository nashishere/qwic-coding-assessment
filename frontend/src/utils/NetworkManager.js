import axios from "axios";

const client = axios.create({
  baseURL: "http://localhost:3001/"
});

const NetworkManager = Object.freeze({
  postReturnRequest: (data) => {
    return client.post("return-requests", data);
  },
});

export default NetworkManager;
