import {Entity, model, property} from '@loopback/repository';

@model()
export class ReturnRequest extends Entity {
  @property({
    id: true,
    type: 'number',
    generated: true,
  })
  id: number;

  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      minLength: 6,
      maxLength: 10,
      errorMessage: 'Serial number should be between 6 and 10 characters.',
    },
  })
  serialNumber: string;

  @property({
    type: 'string',
    required: true,
  })
  reason: string;

  @property({
    type: 'string',
    jsonSchema: {
      maxLength: 240,
      errorMessage: 'Reason description cannot be more than 240 characters.',
    },
  })
  reasonDescription?: string;

  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      maxLength: 6,
      errorMessage: 'Invoice number cannot be more than 6 characters.',
    },
  })
  invoiceNumber: string;

  @property({
    type: 'string',
    jsonSchema: {
      maxLength: 240,
      errorMessage: 'Comments cannot be more than 240 characters.',
    },
  })
  comments?: string;

  constructor(data?: Partial<ReturnRequest>) {
    super(data);
  }
}

export interface ReturnRequestRelations {}

export type ReturnRequestWithRelations = ReturnRequest & ReturnRequestRelations;
