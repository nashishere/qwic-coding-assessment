import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  requestBody,
  response,
} from '@loopback/rest';
import {ReturnRequest} from '../models';
import {ReturnRequestRepository} from '../repositories';

export class ReturnRequestController {
  constructor(
    @repository(ReturnRequestRepository)
    public returnRequestRepository : ReturnRequestRepository,
  ) {}

  @post('/return-requests')
  @response(200, {
    description: 'ReturnRequest model instance',
    content: {'application/json': {schema: getModelSchemaRef(ReturnRequest)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ReturnRequest, {
            title: 'NewReturnRequest',
            exclude: ['id'],
          }),
        },
      },
    })
    returnRequest: Omit<ReturnRequest, 'id'>,
  ): Promise<ReturnRequest> {
    return this.returnRequestRepository.create(returnRequest);
  }

  @get('/return-requests/count')
  @response(200, {
    description: 'ReturnRequest model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(): Promise<Count> {
    return this.returnRequestRepository.count();
  }

  @get('/return-requests')
  @response(200, {
    description: 'Array of ReturnRequest model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(ReturnRequest, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(ReturnRequest, {exclude: 'where'}) filter?: Filter<ReturnRequest>,
  ): Promise<ReturnRequest[]> {
    return this.returnRequestRepository.find(filter);
  }

  @get('/return-requests/{id}')
  @response(200, {
    description: 'ReturnRequest model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(ReturnRequest, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(ReturnRequest, {exclude: 'where'}) filter?: FilterExcludingWhere<ReturnRequest>
  ): Promise<ReturnRequest> {
    return this.returnRequestRepository.findById(id, filter);
  }
}
