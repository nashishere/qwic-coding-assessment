import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DatabaseDataSource} from '../datasources';
import {ReturnRequest, ReturnRequestRelations} from '../models';

export class ReturnRequestRepository extends DefaultCrudRepository<
  ReturnRequest,
  typeof ReturnRequest.prototype.id,
  ReturnRequestRelations
> {
  constructor(
    @inject('datasources.database') dataSource: DatabaseDataSource,
  ) {
    super(ReturnRequest, dataSource);
  }
}
